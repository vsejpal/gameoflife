using GameOfLife.Types;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameOfLife {
    [TestClass]
    public class GameOfLifeUnitTests {
        private readonly IGameOfLife game;

        public GameOfLifeUnitTests()
        {
            // setup DI
             var serviceProvider = new ServiceCollection()
                .AddSingleton<IGameOfLife, GameOfLife>()
                .BuildServiceProvider();
                

            // use DI
            game = serviceProvider.GetService<IGameOfLife>();

        }

        [TestMethod]
        public void GetNextGenerationTest_AlternatePattern_One() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false), new Cell(false), new Cell(false)},
                {new Cell(true), new Cell(true), new Cell(true)},
                {new Cell(false), new Cell(false), new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false), new Cell(true), new Cell(false)},
                {new Cell(false), new Cell(true), new Cell(false)},
                {new Cell(false), new Cell(true), new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_AlternatePattern_Two() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false), new Cell(true), new Cell(false)},
                {new Cell(false), new Cell(true), new Cell(false)},
                {new Cell(false), new Cell(true), new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false), new Cell(false), new Cell(false)},
                {new Cell(true), new Cell(true), new Cell(true)},
                {new Cell(false), new Cell(false), new Cell(false)}
            };


            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_SingleRow_One() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(true), new Cell(true), new Cell(true)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false), new Cell(true), new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);

        }

        [TestMethod]
        public void GetNextGenerationTest_SingleRow_Two() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false), new Cell(true), new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false), new Cell(false), new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_SingleRow_Three() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false), new Cell(false), new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false), new Cell(false), new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_SingleColumn_One() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(true)},
                {new Cell(true)},
                {new Cell(true)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false)},
                {new Cell(true)},
                {new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_SingleColumn_Two() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false)},
                {new Cell(true)},
                {new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false)},
                {new Cell(false)},
                {new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }

        [TestMethod]
        public void GetNextGenerationTest_SingleColumn_Three() {
            // Arrange
            Cell[,] board = new Cell[,] {
                {new Cell(false)},
                {new Cell(false)},
                {new Cell(false)}
            };

            Cell[,] expectedNewGeneration = new Cell[,] {
                {new Cell(false)},
                {new Cell(false)},
                {new Cell(false)}
            };

            // Act
            Cell[,] actualNewGeneration = game.GetNextGeneration(board);

            // Assert
            CollectionAssert.AreEqual(expectedNewGeneration, actualNewGeneration);
        }
    }
}
