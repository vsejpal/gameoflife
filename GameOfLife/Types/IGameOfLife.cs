﻿using GameOfLife.Types;

namespace GameOfLife
{
    public interface IGameOfLife
    {
        Cell[,] GetNextGeneration(Cell[,] board);

        void PrintBoard();

        void AdvanceGeneration();
    }
}