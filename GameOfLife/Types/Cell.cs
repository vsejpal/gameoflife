﻿using System;
namespace GameOfLife.Types
{
    public struct Cell
    {
        public bool isAlive;
        public Cell(bool isAlive = false) => this.isAlive = isAlive;
    }
}
