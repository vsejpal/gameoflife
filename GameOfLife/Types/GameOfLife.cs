﻿using System;
using System.IO;
using GameOfLife.Types;

namespace GameOfLife
{
    public class GameOfLife : IGameOfLife
    {
        private Cell[,] _board;
        public GameOfLife(string filePath = null)
        {
            if (!string.IsNullOrWhiteSpace(filePath)) {
                _board = InitializeBoard(filePath);
            }
        }

        public Cell[,] GetNextGeneration(Cell[,] board)
        {
            Cell[,] newBoard = new Cell[board.GetLength(0), board.GetLength(1)];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    ushort liveNeighborCount = CountLiveNeighbors(i, j, board);

                    // rule to live on to next generation
                    if ((liveNeighborCount == 2 || liveNeighborCount == 3) && board[i, j].isAlive)
                    {
                        newBoard[i, j].isAlive = true;
                    }

                    // rule to live on to next generation
                    if (liveNeighborCount == 3 && !board[i, j].isAlive)
                    {
                        newBoard[i, j].isAlive = true;
                    }
                }
            }
            return newBoard;
        }

        public void PrintBoard()
        {
            for (int i = 0; i < _board.GetLength(0); i++)
            {
                for (int j = 0; j < _board.GetLength(1); j++)
                {
                    char p = _board[i, j].isAlive ? '1' : '.';
                    Console.Write("{0} ", p);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void AdvanceGeneration()
        {
            _board = GetNextGeneration(_board);
        }

        private Cell[,] InitializeBoard(string inputFile)
        {
            string[] inputLines = File.ReadAllLines(inputFile);

            int rowCount = inputLines.Length;
            int colCount = inputLines[0].Length;

            Cell[,] board = new Cell[rowCount, colCount];

            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < colCount; j++)
                {
                    board[i, j].isAlive = (inputLines[i][j] == '1');
                }
            }

            return board;

        }

        private byte CountLiveNeighbors(int row, int col, Cell[,] board)
        {
            byte liveNeighborCount = 0;
            // look up
            if (row > 0)
            {
                liveNeighborCount = Convert.ToByte((board[row - 1, col].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);

                // look up-left
                if (col > 0)
                {
                    liveNeighborCount = Convert.ToByte((board[row - 1, col - 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
                }
                // look up-right
                if (col < board.GetLength(1) - 1)
                {
                    liveNeighborCount = Convert.ToByte((board[row - 1, col + 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
                }
            }

            // look down
            if (row < board.GetLength(0) - 1)
            {
                liveNeighborCount = Convert.ToByte((board[row + 1, col].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);

                // look down-left
                if (col > 0)
                {
                    liveNeighborCount = Convert.ToByte((board[row + 1, col - 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
                }
                // look down-right
                if (col < board.GetLength(1) - 1)
                {
                    liveNeighborCount = Convert.ToByte((board[row + 1, col + 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
                }
            }

            //look left
            if (col > 0)
            {
                liveNeighborCount = Convert.ToByte((board[row, col - 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
            }

            // look right
            if (col < board.GetLength(1) - 1)
            {
                liveNeighborCount = Convert.ToByte((board[row, col + 1].isAlive) ? liveNeighborCount + 1 : liveNeighborCount);
            }
            return liveNeighborCount;
        }
    }
}
