﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace GameOfLife
{
    public class Program {
        private static readonly IGameOfLife game;

        static Program() {
            // setup DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IGameOfLife>(s => new GameOfLife("./board-setup/RandomPattern.txt"))
               .BuildServiceProvider();


            // use DI
            game = serviceProvider.GetService<IGameOfLife>();
        }

        static void Main(string[] args) {
            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += (s, ev) => {
                ev.Cancel = true;
            };

            int generation = 0;
            do {
                Console.Clear();
                Console.WriteLine("Generation {0}", generation);
                game.PrintBoard();
                Console.WriteLine("Press X to quit, any other key to continue.");
                generation++;
                game.AdvanceGeneration();
            } while (Console.ReadKey().Key != ConsoleKey.X);

        }
    }
}
