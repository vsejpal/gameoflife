**Game Of Life**

To Run:
 - `git clone https://vsejpal@bitbucket.org/vsejpal/gameoflife.git`

- `cd GameOfLife/GameOfLife`

- `dotnet build`

- `dotnet run`

To change initial board Setup:

- add a new file in `GameOfLife/board-setup/`

- Edit `GameOfLife/Program.cs` to refer to this new text file

